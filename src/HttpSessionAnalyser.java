import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;


public interface HttpSessionAnalyser{
	
	public void handleHttpSession(HttpRequest request, HttpResponse response);
}
