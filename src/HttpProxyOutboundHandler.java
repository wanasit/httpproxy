import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.handler.codec.http.HttpChunk;
import org.jboss.netty.handler.codec.http.HttpResponse;

public class HttpProxyOutboundHandler extends SimpleChannelUpstreamHandler {
	

    protected HttpProxyInboundHandler inboundHandler;
    public HttpProxyOutboundHandler(HttpProxyInboundHandler inboundHandler) {
        this.inboundHandler = inboundHandler;
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        Object message = e.getMessage();
        
    	if(message instanceof HttpResponse){
    		
    		HttpResponse response = (HttpResponse) message;
    		this.inboundHandler.writeResponse(response);
    		
    	}else if(message instanceof HttpChunk){
    		
    		HttpChunk chunk = (HttpChunk) message;
    		this.inboundHandler.writeChunk(chunk);
    	}	
    }
    
    
    public void channelInterestChanged(ChannelHandlerContext ctx,
        ChannelStateEvent e) throws Exception {
    	if (e.getChannel().isWritable()) {
            //o.setReadable(true);
        }
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        //closeOnFlush(inboundChannel);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e)
            throws Exception {
        e.getCause().printStackTrace();
        //closeOnFlush(e.getChannel());
    }
    
    /**
     * Closes the specified channel after all queued write requests are flushed.
     */
    static void closeOnFlush(Channel ch) {
        if (ch.isConnected()) {
            ch.write(ChannelBuffers.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
        }
    }
}

