
import static org.jboss.netty.channel.Channels.*;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.handler.codec.http.*;

public class HttpProxyOutboundPipelineFactory implements ChannelPipelineFactory {
	
	protected HttpProxyInboundHandler inboundHandler;
    public HttpProxyOutboundPipelineFactory(HttpProxyInboundHandler inboundHandler) {
        this.inboundHandler = inboundHandler;
    }
	
    public ChannelPipeline getPipeline() throws Exception {
        // Create a default pipeline implementation.
        ChannelPipeline pipeline = pipeline();
        
        // The channel' upstream : 'HttpRequest' -> 'binary' 
        pipeline.addLast("encoder", new HttpRequestEncoder());
        
        // The channel's downstream : 'binary' -> 'HttpRequest' 
        pipeline.addLast("decoder", new HttpResponseDecoder());
        
        pipeline.addLast("handler", new HttpProxyOutboundHandler(this.inboundHandler));
        return pipeline;
    }
}
