import static org.jboss.netty.handler.codec.http.HttpHeaders.getHost;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.QueryStringDecoder;


public class LoggingAnalyser implements HttpSessionAnalyser {
	
	@Override
	public void handleHttpSession(HttpRequest request, HttpResponse response) {
		
		// TODO Auto-generated method stub
		System.out.println("=========== REQUEST ============");
		System.out.println("VERSION: " + request.getProtocolVersion());
		System.out.println("HOSTNAME: " + getHost(request, "unknown"));
		System.out.println("REQUEST_URI: " + request.getUri());
		for (Map.Entry<String, String> h: request.getHeaders()) {
			System.out.println("HEADER: " + h.getKey() + " = " + h.getValue());
		}
		
		QueryStringDecoder queryStringDecoder = new QueryStringDecoder(request.getUri());
		Map<String, List<String>> params = queryStringDecoder.getParameters();
		if (!params.isEmpty()) {
		    for (Entry<String, List<String>> p: params.entrySet()) {
		        String key = p.getKey();
		        List<String> vals = p.getValue();
		        for (String val : vals) {
		        	System.out.println("PARAM: " + key + " = " + val);
		        }
		    }
		}
		
		System.out.println("=========== RESPONSE ============");
		System.out.println("STATUS: " + response.getStatus().getCode());
		for (Map.Entry<String, String> h: response.getHeaders()) {
			System.out.println("HEADER: " + h.getKey() + " = " + h.getValue());
		}

		
	}


}
