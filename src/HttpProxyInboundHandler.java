/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */


import static org.jboss.netty.handler.codec.http.HttpHeaders.*;
import static org.jboss.netty.handler.codec.http.HttpResponseStatus.*;
import static org.jboss.netty.handler.codec.http.HttpVersion.*;

import java.net.InetSocketAddress;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.jboss.netty.handler.codec.http.*;


public class HttpProxyInboundHandler extends SimpleChannelUpstreamHandler {
	
    private HttpRequest request;
    private boolean readingChunks;
    
    private volatile Channel outboundChannel;
    private volatile Channel inboundChannel;
    private List<HttpChunk> chunkBuffer = new LinkedList<HttpChunk>();
        
    private List<HttpSessionAnalyser> analyzers = new LinkedList<HttpSessionAnalyser>();
    public List<HttpSessionAnalyser> getAnalyzers() {
		return analyzers;
	}
    
    protected void handleRequest(final HttpRequest request) throws Exception {
    	
        Executor executor = Executors.newSingleThreadExecutor();
        ClientBootstrap outboundBootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(
        		executor, executor));
        outboundBootstrap.setPipelineFactory(new HttpProxyOutboundPipelineFactory(this));
        
        //
        java.net.URL url = new java.net.URL("http://"+getHost(request, "unknown"));
        int port = url.getPort() > 0 ? url.getPort() : 80;
        
        ChannelFuture f = outboundBootstrap.connect(new InetSocketAddress(url.getHost(), port));
        inboundChannel.setReadable(false);
        outboundChannel = f.getChannel();
        
        f.addListener(new ChannelFutureListener() {
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                	
                	outboundChannel.write(request);
                	for (HttpChunk chunk : chunkBuffer) {
                		outboundChannel.write(chunk);
					}
                	
                	chunkBuffer.clear();
                    inboundChannel.setReadable(true);
                } else {
                    inboundChannel.close();
                }
            }
        });
        
        if (request.isChunked()) {
            readingChunks = true;
        }
    } 
    
    protected void handleChunk(HttpChunk chunk){
    	
    	if(outboundChannel.isConnected()) {
        	outboundChannel.write(chunk);
        }
        else{
        	chunkBuffer.add(chunk);
        }
        
        if (chunk.isLast()) {
            readingChunks = false;
        }
    }
    
    public void writeResponse(HttpResponse response) {
    	
    	for (HttpSessionAnalyser analyser : this.getAnalyzers()) {
    		analyser.handleHttpSession(this.request, response);
		}
    	
    	this.inboundChannel.write(response);
    }
    
    public void writeChunk(HttpChunk chunk) {
    	this.inboundChannel.write(chunk);
    }
    
    private static void send100Continue(MessageEvent e) {
        HttpResponse response = new DefaultHttpResponse(HTTP_1_1, CONTINUE);
        e.getChannel().write(response);
    }

    /**
     * 
     */
    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        
    	if (!readingChunks) {
        	
    		HttpRequest request = (HttpRequest) e.getMessage();
    		if (is100ContinueExpected(request)) {
                send100Continue(e);
                return;
            }
    		
    		this.inboundChannel = e.getChannel();
    		this.request = request;
    		this.handleRequest(request);
            
        } else {
            HttpChunk chunk = (HttpChunk) e.getMessage();
            this.handleChunk(chunk);
        }
    }
    
    /**
     * 
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e)
            throws Exception {
        e.getCause().printStackTrace();
        e.getChannel().close();
    }

	


}
