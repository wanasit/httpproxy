import static org.jboss.netty.channel.Channels.*;

import java.util.LinkedList;
import java.util.List;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.handler.codec.http.*;
import org.jboss.netty.handler.ipfilter.IpFilterRuleHandler;
import org.jboss.netty.handler.ipfilter.IpFilterRuleList;

public class HttpProxyInboundPipelineFactory implements ChannelPipelineFactory {
	
	private List<HttpSessionAnalyser> analyzers = new LinkedList<HttpSessionAnalyser>();
    public List<HttpSessionAnalyser> getAnalyzers() {
		return analyzers;
	}
    
    public ChannelPipeline getPipeline() throws Exception {
        
        ChannelPipeline pipeline = pipeline();
        
        //Filter localhost
        pipeline.addFirst("filter", new IpFilterRuleHandler(new IpFilterRuleList("-n:localhost, -i:127.0.0.1")));
        
        // The channel' upstream : 'binary' -> 'HttpRequest'
        pipeline.addLast("decoder", new HttpRequestDecoder());
        
        // The channel's downstream :  'HttpRespone' -> 'binary'
        pipeline.addLast("encoder", new HttpResponseEncoder());
        
        HttpProxyInboundHandler handler = new HttpProxyInboundHandler();
        handler.getAnalyzers().addAll(this.getAnalyzers());
        
        pipeline.addLast("handler", handler);
        return pipeline;
    }
}
